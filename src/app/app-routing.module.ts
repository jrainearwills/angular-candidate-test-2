import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { CitationsResultsComponent } from './pages/citations/results/citations-results.component';
import { CitationDetailsComponent } from './pages/citations/details/citation-details/citation-details.component';
import { DetailsModule } from './pages/citations/details/details.module';
import { ResultsModule } from './pages/citations/results/results.module';

const routes: Routes = [
  { path: '', component: HomeComponent, title: 'Home' },
  { path: 'results', component: CitationsResultsComponent, title: 'NTRS - Search Results' },
  { path: 'citation/:id', component: CitationDetailsComponent, title: 'NTRS - Document Details' }
];

@NgModule({
  imports: [DetailsModule, ResultsModule, RouterModule.forRoot(routes, { bindToComponentInputs: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
