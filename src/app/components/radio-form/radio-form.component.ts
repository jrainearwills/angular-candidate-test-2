import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, map } from 'rxjs';
import { AutocompleteService } from 'src/app/shared-services/autocomplete.service';

@Component({
  selector: 'app-radio-form',
  templateUrl: './radio-form.component.html',
  styleUrls: ['./radio-form.component.scss']
})
export class RadioFormComponent implements OnInit {
  @Input()
  title!: string;

  @Input()
  itemControl!: FormControl<string>;

  @Input()
  optionCategory!: string;

  @Input()
  defaultValue: string | null = null;

  options!: Observable<string[]>

  constructor(private service: AutocompleteService) { }

  ngOnInit(): void {
    this.options = this.service.getOptions<string>(this.optionCategory).pipe(map(options => {
      return this.defaultValue ? [this.defaultValue].concat(options) : options
    }));
  }
}
