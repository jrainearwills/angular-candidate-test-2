import { Component, Input } from '@angular/core';
import { Citation } from 'src/app/pages/citations/models';
import { QueryParamService } from 'src/app/shared-services/query-param.service';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.scss']
})
export class AuthorsComponent {
  @Input()
  citation!: Citation

  constructor(private paramsService: QueryParamService) { }

  appendAuthorFilter = (author: string): void => {
    this.paramsService.appendAuthor(author);
  }

  getAuthors = (): string[] => {
    return this.citation.authorAffiliations?.map(af => af.meta.author.name) || []
  }
}
