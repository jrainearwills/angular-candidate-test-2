import { Component, Input } from '@angular/core';
import { Citation } from 'src/app/pages/citations/models';

@Component({
  selector: 'app-document-title',
  templateUrl: './document-title.component.html',
  styleUrls: ['./document-title.component.scss']
})
export class DocumentTitleComponent {
  @Input()
  citation!: Citation;

  @Input()
  disableRouter: boolean = false;
}
