import { Component, Input } from '@angular/core';
import { Citation, Funding } from 'src/app/pages/citations/models';

@Component({
  selector: 'app-funding',
  templateUrl: './funding.component.html',
  styleUrls: ['./funding.component.scss']
})
export class FundingComponent {
  @Input()
  citation!: Citation;

  getFunding = (): Funding[] => {
    return this.citation?.fundingNumbers || []
  }
}
