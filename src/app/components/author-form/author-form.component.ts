import { Component, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { NbTagComponent, NbTagInputAddEvent } from '@nebular/theme';

@Component({
  selector: 'app-author-form',
  templateUrl: './author-form.component.html',
  styleUrls: ['./author-form.component.scss']
})
export class AuthorFormComponent {
  @Input()
  authorsForm!: FormControl<string[]>;

  keyCodes = [188, 13, 9];

  handleAddAuthor = ({ value, input }: NbTagInputAddEvent) => {
    if (Boolean(value) && !this.authorsForm.value.includes(value)) {
      const newValue = [...this.authorsForm.value, value];
      this.authorsForm.setValue(newValue)
    }
    setTimeout(() => input.nativeElement.value = '', 1)
  }

  handleRemoveAuthor = (tag: NbTagComponent) => {
    let authors = this.authorsForm.value;
    this.authorsForm.setValue(authors.filter(a => a !== tag.text));
  }
}
