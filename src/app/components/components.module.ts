import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchBoxComponent } from './search-box/search-box.component';
import { AppHeaderComponent } from './app-header/app-header.component';
import { AuthorFormComponent } from './author-form/author-form.component';
import { ChecklistFormComponent } from './checklist-form/checklist-form.component';
import { DateRangeFormComponent } from './date-range-form/date-range-form.component';
import { RadioFormComponent } from './radio-form/radio-form.component';
import { PublicationsComponent } from './publications/publications.component';
import { AuthorsComponent } from './authors/authors.component';
import { FundingComponent } from './funding/funding.component';
import { KeywordsComponent } from './keywords/keywords.component';
import { DocumentTypeComponent } from './document-type/document-type.component';
import { DocumentTitleComponent } from './document-title/document-title.component';
import { PdfComponent } from './pdf/pdf.component';
import { NbAccordionModule, NbButtonModule, NbCheckboxModule, NbDatepickerModule, NbFormFieldModule, NbIconModule, NbInputModule, NbRadioModule, NbSpinnerModule, NbTagModule, NbThemeModule } from '@nebular/theme';
import { ReactiveFormsModule } from '@angular/forms';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { RouterModule } from '@angular/router';
import { NbEvaIconsModule } from '@nebular/eva-icons';



@NgModule({
  declarations: [
    AppHeaderComponent,
    AuthorFormComponent,
    AuthorsComponent,
    ChecklistFormComponent,
    DateRangeFormComponent,
    DocumentTitleComponent,
    DocumentTypeComponent,
    FundingComponent,
    KeywordsComponent,
    PdfComponent,
    PublicationsComponent,
    RadioFormComponent,
    SearchBoxComponent,
  ],
  exports: [
    AppHeaderComponent,
    AuthorFormComponent,
    AuthorsComponent,
    ChecklistFormComponent,
    DateRangeFormComponent,
    DocumentTitleComponent,
    DocumentTypeComponent,
    FundingComponent,
    KeywordsComponent,
    PdfComponent,
    PublicationsComponent,
    RadioFormComponent,
    SearchBoxComponent,
  ],
  imports: [
    CommonModule,
    NbButtonModule,
    NbInputModule,
    NbIconModule,
    NbEvaIconsModule,
    ReactiveFormsModule,
    NbFormFieldModule,
    NbRadioModule,
    NbAccordionModule,
    PdfViewerModule,
    NbSpinnerModule,
    RouterModule,
    NbDatepickerModule,
    NbCheckboxModule,
    NbTagModule,
  ]
})
export class ComponentsModule { }
