import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, map, take } from 'rxjs';
import { ActivatedRoute, ParamMap, Params, Router } from '@angular/router';
import { SearchOption } from 'src/app/models/search';
import { QueryParamService } from 'src/app/shared-services/query-param.service';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {
  @Input() width = '60%';
  inputFormControl = new FormControl<string>("")

  constructor(private paramsService: QueryParamService) { }

  @Input()
  showLabel: boolean = true;

  ngOnInit(): void {
    this.paramsService.params.pipe(take(1)).subscribe(params => this.inputFormControl.setValue(params.q))
  }

  handleEnter = () => this.navigateToCitations();
  handleClick = () => this.navigateToCitations();

  private navigateToCitations = () => {
    this.paramsService.appendQueryField(this.inputFormControl.value)
  }
}
