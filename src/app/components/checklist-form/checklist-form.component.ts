import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable, map, take } from 'rxjs';
import { AutocompleteService } from 'src/app/shared-services/autocomplete.service';

@Component({
  selector: 'app-checklist-form',
  templateUrl: './checklist-form.component.html',
  styleUrls: ['./checklist-form.component.scss']
})
export class ChecklistFormComponent<T> implements OnInit {
  @Input()
  title!: string

  @Input()
  itemCategory!: string;

  @Input()
  itemsControl!: FormControl<T[]>

  options!: Observable<T[]>

  constructor(private optionsService: AutocompleteService) { }

  ngOnInit(): void {
    this.options = this.optionsService.getOptions<T>(this.itemCategory).pipe(take(1));
  }

  handleCheck = (isChecked: boolean, item: T) => {
    if (isChecked) {
      this.itemsControl.setValue([...this.itemsControl.value, item])
    } else {
      this.itemsControl.setValue(this.itemsControl.value.filter(o => o !== item))
    }
  }

  isChecked = (option: T): boolean => this.itemsControl?.value?.includes(option) ?? false
}
