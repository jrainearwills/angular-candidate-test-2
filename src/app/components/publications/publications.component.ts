import { Component, Input } from '@angular/core';
import { NbDateFnsDateService } from '@nebular/date-fns';
import { Citation } from 'src/app/pages/citations/models';

@Component({
  selector: 'app-publications',
  templateUrl: './publications.component.html',
  styleUrls: ['./publications.component.scss']
})
export class PublicationsComponent {
  @Input()
  citation!: Citation;

  @Input()
  displayPublisher: boolean = false;

  constructor(private dateService: NbDateFnsDateService) { }

  getPublications = (): string[] => {
    return this.citation.publications.map(pub => {
      const publication = []
      if (pub.publicationName || pub.publisher) {
        publication.push(pub.publicationName || pub.publisher)
      }

      if (pub.volume) {
        publication.push(`Vol. ${pub.volume}`)
      }

      if (pub.issue) {
        publication.push(`No. ${pub.issue}`)
      }

      if (pub.publicationDate) {
        publication[publication.length - 1] += ` (${this.dateService.format(new Date(pub.publicationDate), 'MMMM yyyy')})`;
      }

      if (this.displayPublisher && pub.publisher) {
        publication.push(`Published By: ${pub.publisher}`)
      }

      return publication.join(', ')
    })
  }
}
