import { Component, Input } from '@angular/core';
import { PDFProgressData } from 'ng2-pdf-viewer';

@Component({
  selector: 'app-pdf',
  templateUrl: './pdf.component.html',
  styleUrls: ['./pdf.component.scss']
})
export class PdfComponent {
  @Input()
  url!: string

  @Input()
  downloadFilename?: string;

  loading = false;

  onProgress = (progress: PDFProgressData): void => {
    this.loading = progress.loaded < progress.total;
  }
}
