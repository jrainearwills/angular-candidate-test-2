import { Component, Input } from '@angular/core';
import { Citation } from 'src/app/pages/citations/models';

@Component({
  selector: 'app-keywords',
  templateUrl: './keywords.component.html',
  styleUrls: ['./keywords.component.scss']
})
export class KeywordsComponent {
  @Input()
  citation!: Citation

  getKeywords = () => {
    return this.citation?.keywords || []
  }
}
