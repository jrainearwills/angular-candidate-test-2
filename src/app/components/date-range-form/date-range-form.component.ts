import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-date-range-form',
  templateUrl: './date-range-form.component.html',
  styleUrls: ['./date-range-form.component.scss']
})
export class DateRangeFormComponent implements OnInit {
  @Input()
  dateFormGroup!: FormGroup;

  @Input()
  title!: string;

  private startDate!: Date;
  private endDate!: Date;

  constructor() { }

  ngOnInit(): void {
    const startControl = this.dateFormGroup.get('start');
    const endControl = this.dateFormGroup.get('end');

    startControl?.valueChanges.subscribe(value => this.startDate = value);
    endControl?.valueChanges.subscribe(value => this.endDate = value);
  }

  filterStartDate = (date: Date): boolean => {
    return !this.endDate || date <= this.endDate;
  }

  filterEndDate = (date: Date): boolean => {
    return !this.startDate || date >= this.startDate;
  }
}
