import { Component, Input } from '@angular/core';
import { Citation } from 'src/app/pages/citations/models';

@Component({
  selector: 'app-document-type',
  templateUrl: './document-type.component.html',
  styleUrls: ['./document-type.component.scss']
})
export class DocumentTypeComponent {
  @Input()
  citation!: Citation;
}
