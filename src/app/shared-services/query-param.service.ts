import { Injectable } from '@angular/core';
import { ActivatedRoute, NavigationEnd, NavigationSkipped, Params, Router } from '@angular/router';
import { NbDateFnsDateService } from '@nebular/date-fns';
import { filter, map } from 'rxjs';
import { CitationsParams, CitationsForm } from '../pages/citations/models';

@Injectable({
  providedIn: 'root'
})
export class QueryParamService {
  constructor(private router: Router, private activatedRoute: ActivatedRoute, private dateService: NbDateFnsDateService) { }

  get params() {
    return this.activatedRoute.queryParams.pipe(map(params => {
      return params as CitationsParams
    }))
  }

  appendAuthor(author: string) {
    const previous = this.activatedRoute.snapshot.queryParams['author'];
    const authors: string[] = previous instanceof Array ? previous : Boolean(previous) ? [previous] : [];

    const params = {
      author: !authors.includes(author) ? authors.concat(author) : authors
    }

    this.navigate(params);
  }

  appendQueryField(value: string | null) {
    const params = {
      q: value || null
    }
    this.navigate(params);
  }

  appendCitationsForm = (form: CitationsForm) => {
    const params = {
      author: form.author,
      organization: form.organization,
      title: form.title || null,
      stiTypeDetails: form.stiTypeDetails || null,
      publishedStart: this.getFormattedDate(form.published.start),
      publishedEnd: this.getFormattedDate(form.published.end),
      createdStart: this.getFormattedDate(form.created.start),
      createdEnd: this.getFormattedDate(form.created.end),
    }

    this.navigate(params);
  }

  private getFormattedDate = (date: Date | null) => {
    return date ? this.dateService.format(date, 'yyyy/MM/dd') : null
  }

  private navigate = (params: { [key: string]: unknown }) => {
    this.router.navigate(['/results'], {
      queryParams: params,
      queryParamsHandling: 'merge'
    })
  }
}
