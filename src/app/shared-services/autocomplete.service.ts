import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutocompleteService {
  constructor(private client: HttpClient) { }

  getOptions = <T>(category: string): Observable<T[]> => this.client.request<T[]>('GET', '/api/citations/autocomplete', {
    headers: {
      'accept': "application/json",
    },
    params: {
      field: category,
      q: ""
    }
  }).pipe(map<T[], T[]>(orgs => {
    const sorted = orgs.sort((a, b) => {
      if (!Boolean(a)) return 1
      if (!Boolean(b)) return -1
      return a < b ? -1 : 1
    });

    while (sorted.length > 0 && !Boolean(sorted[sorted.length - 1])) {
      sorted.pop()
    }

    return sorted;
  }))
}
