import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NbDatepickerModule, NbLayoutModule, NbThemeModule, NbSidebarModule } from '@nebular/theme';
import { NbDateFnsDateModule, NbDateFnsDateService } from '@nebular/date-fns';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsModule } from './components/components.module';




@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    ComponentsModule,
    HttpClientModule,
    NbDateFnsDateModule.forRoot({ format: "yyyy/MM/dd" }),
    NbDatepickerModule.forRoot(),
    NbEvaIconsModule,
    NbLayoutModule,
    NbSidebarModule.forRoot(),
    NbThemeModule.forRoot({ name: 'default' }),
  ],
  providers: [NbDateFnsDateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
