export interface SearchOption {
    display: string,
    value: [string, string | null],
}