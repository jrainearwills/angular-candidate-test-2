import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, Subscription, defaultIfEmpty, map, of, startWith, switchMap, take } from 'rxjs';
import { QueryParamService } from 'src/app/shared-services/query-param.service';
import { Citation, CitationsForm, CitationsParams, CitationsRequestBody, CitationsResult } from './models';

@Injectable({
  providedIn: 'root'
})
export class CitationsService {
  private _citations = new BehaviorSubject<CitationsResult | null>(null);
  private _loading = new BehaviorSubject<boolean>(false);

  constructor(private httpClient: HttpClient, private paramsService: QueryParamService) {
    this.paramsService.params.pipe(map<CitationsParams, CitationsRequestBody>(params => ({
      distribution: "PUBLIC",
      q: params.q,
      author: params.author,
      organization: params.organization,
      title: params.title,
      stiTypeDetails: params.stiTypeDetails,
      published: {
        gte: params.publishedStart,
        lte: params.publishedEnd,
        format: "yyyy/MM/dd"
      },
      created: {
        gte: params.createdStart,
        lte: params.createdEnd,
        format: "yyyy/MM/dd"
      },
    }))).subscribe(this.search)
  }

  get citations() {
    return this._citations
  }

  get loading() {
    return this._loading
  }

  getCitation = (citationId: number | null): Observable<Citation> => {
    return this._citations.pipe(switchMap(citations => {
      const citation = citations?.results?.find(c => c.id == citationId);
      return citation ? of(citation) : this.httpClient.request<Citation>('GET', `/api/citations/${citationId}`)
    }))
  }

  search = (search: CitationsRequestBody): void => {
    this._loading.next(true);
    this.httpClient.request<CitationsResult>('POST', '/api/citations/search', {
      body: search
    }).subscribe(result => {
      this._citations.next(result);
      this._loading.next(false);
    })
  }
}
