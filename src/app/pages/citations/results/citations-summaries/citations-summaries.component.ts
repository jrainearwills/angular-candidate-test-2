import { Component, OnDestroy, OnInit } from '@angular/core';
import { CitationsService } from '../../citations.service';
import { Subscription } from 'rxjs';
import { Aggregations, Citation, CitationDownload, Stats } from '../../models';
import { NbDateFnsDateService } from '@nebular/date-fns';
import { QueryParamService } from 'src/app/shared-services/query-param.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-citations-summaries',
  templateUrl: './citations-summaries.component.html',
  styleUrls: ['./citations-summaries.component.scss']
})
export class CitationsSummariesComponent implements OnInit, OnDestroy {
  meta: Stats = {} as Stats;
  citations: Citation[] = [];
  aggregations: Aggregations = {} as Aggregations;
  private subscription!: Subscription;

  constructor(private router: Router, private citationsService: CitationsService, private dateService: NbDateFnsDateService, private paramsService: QueryParamService) { }

  ngOnInit(): void {
    this.subscription = this.citationsService.citations.subscribe(citationsResult => {
      this.meta = citationsResult?.stats || {} as Stats;
      this.citations = citationsResult?.results || [];
      this.aggregations = citationsResult?.aggregations || {};
    })
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  getDownloadLink = (download: CitationDownload) => {
    return download.links.pdf || download.links.original
  }

  getFileExt = (download: CitationDownload) => {
    const url = this.getDownloadLink(download);
    const ext = /(?<ext>\.\w+)$/.exec(url)?.groups?.['ext'];
    return ext
  }

  handleDetailsClick = (id: number) => {
    this.router.navigate(['/citation', id])
  }

  replaceWhitespace = (value: string) => value.replace(/\s+/g, '_');
}
