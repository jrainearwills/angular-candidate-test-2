import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CitationsService } from '../citations.service';
import { Meta, Title } from '@angular/platform-browser';

@Component({
  selector: 'app-citations-results',
  templateUrl: './citations-results.component.html',
  styleUrls: ['./citations-results.component.scss']
})
export class CitationsResultsComponent implements OnInit, OnDestroy {
  loading = false;
  private loadingSubscription: Subscription;
  constructor(service: CitationsService, private title: Title, private meta: Meta) {
    this.loadingSubscription = service.loading.subscribe(loading => this.loading = loading);
  }

  ngOnInit(): void {
    this.meta.updateTag({ name: 'description', content: 'NTRS public search results list of document summaries' })
  }

  ngOnDestroy(): void {
    this.loadingSubscription.unsubscribe();
  }
}
