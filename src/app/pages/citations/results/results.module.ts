import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitationDetailsComponent } from '../details/citation-details/citation-details.component';
import { CitationsFormComponent } from './citations-form/citations-form.component';
import { CitationsSummariesComponent } from './citations-summaries/citations-summaries.component';
import { CitationsResultsComponent } from './citations-results.component';
import { NbButtonModule, NbFormFieldModule, NbIconModule, NbInputModule, NbLayoutModule, NbSidebarModule, NbSpinnerModule } from '@nebular/theme';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from 'src/app/components/components.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    CitationsFormComponent,
    CitationsSummariesComponent,
    CitationsResultsComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    NbButtonModule,
    NbFormFieldModule,
    NbInputModule,
    NbIconModule,
    NbLayoutModule,
    NbSidebarModule,
    NbSpinnerModule,
    ReactiveFormsModule,
    RouterModule,
  ]
})
export class ResultsModule { }
