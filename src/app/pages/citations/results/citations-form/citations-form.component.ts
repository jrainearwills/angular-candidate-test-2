import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { QueryParamService } from 'src/app/shared-services/query-param.service';
import { CitationsService } from '../../citations.service';
import { debounce, interval, map, take } from 'rxjs';
import { CitationsForm } from '../../models';

@Component({
  selector: 'app-citations-form',
  templateUrl: './citations-form.component.html',
  styleUrls: ['./citations-form.component.scss']
})
export class CitationsFormComponent {
  searchForm!: FormGroup;
  defaultStiType = 'Any';

  constructor(private formBuilder: FormBuilder, private paramsService: QueryParamService, private citationsService: CitationsService) { }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      author: [[]],
      organization: [[]],
      title: [null],
      published: this.formBuilder.group({
        start: null,
        end: null,
      }),
      created: this.formBuilder.group({
        start: null,
        end: null
      }),
      stiTypeDetails: [this.defaultStiType]
    })

    this.paramsService.params.subscribe(params => {
      this.searchForm.patchValue({
        author: this.getArrayParam(params.author),
        organization: this.getArrayParam(params.organization),
        title: params.title,
        published: {
          start: params.publishedStart ? new Date(params.publishedStart) : null,
          end: params.publishedEnd ? new Date(params.publishedEnd) : null,
        },
        created: {
          start: params.createdStart ? new Date(params.createdStart) : null,
          end: params.createdEnd ? new Date(params.createdEnd) : null,
        },
        stiTypeDetails: params.stiTypeDetails || this.defaultStiType
      }, {
        emitEvent: false
      })
    })

    this.searchForm.valueChanges.pipe(debounce(() => interval(500)), map<CitationsForm, CitationsForm>(data => {
      if (data.stiTypeDetails === this.defaultStiType) {
        data.stiTypeDetails = null
      }
      return data;
    })).subscribe(this.paramsService.appendCitationsForm);
  }

  getControl = (controlName: string) => this.searchForm.get(controlName) as FormControl

  getGroup = (groupName: string) => this.searchForm.get(groupName) as FormGroup

  hasValue = (controlName: string) => Boolean(this.searchForm.get(controlName)?.value)

  setValue = (controlName: string, value: unknown) => this.searchForm.get(controlName)?.setValue(value)

  getArrayParam = (value: string | string[]): string[] => {
    if (!Boolean(value)) return []
    if (value instanceof Array) return value;
    return [value]
  }
}
