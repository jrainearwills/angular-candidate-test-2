export interface CitationsForm {
    author: string[],
    organization: string[],
    title: string,
    published: {
        start: Date | null,
        end: Date | null
    },
    created: {
        start: Date | null,
        end: Date | null
    },
    stiTypeDetails: string | null
}

export interface CitationsParams extends Omit<CitationsForm, 'published' | 'created'> {
    q: string | null
    publishedStart: string | null,
    publishedEnd: string | null,
    createdStart: string | null,
    createdEnd: string | null
}

export interface CitationsRequestBody extends Omit<CitationsParams, 'publishedStart' | 'publishedEnd' | 'createdStart' | 'createdEnd'> {
    distribution: "PUBLIC",
    created: {
        gte: string | null,
        lte: string | null,
        format: string
    },
    published: {
        gte: string | null,
        lte: string | null
        format: string
    }
}

export interface CitationsResult {
    stats: Stats
    results: Citation[]
    aggregations: Aggregations
}

export interface Stats {
    took: number
    total: number
    estimate: boolean
    maxScore: number
}

export interface Citation {
    _meta: CitationMeta
    copyright: Copyright
    keywords: string[]
    subjectCategories: string[]
    exportControl: ExportControl
    distributionDate: string
    title: string
    stiType: string
    distribution: string
    submittedDate: string
    authorAffiliations: AuthorAffiliation[]
    stiTypeDetails: string
    technicalReviewType: string
    modified: string
    id: number
    legacyMeta: LegacyMeta
    created: string
    center: Center
    onlyAbstract: boolean
    sensitiveInformation: number
    abstract: string
    isLessonsLearned: boolean
    disseminated: string
    meetings: Meeting[]
    publications: Publication[]
    status: string
    related: Related[]
    downloads: CitationDownload[]
    downloadsAvailable: boolean
    index: string
    fundingNumbers?: Funding[]
}

export interface Funding {
    type: string;
    number: string;
}

export interface Related {
    type: string,
    stiType: string,
    disseminated: string,
    distribution: string,
    status: string,
    id: number,
    accessionNumber: string,
    title: string
}

export interface CitationDownload {
    type: string,
    draft: boolean,
    links: {
        original: string,
        pdf?: string,
        fulltext: string,
    },
    mimetype: string,
    name: string,
    notes: string
}

export interface CitationMeta {
    score: number
}

export interface Copyright {
    licenseType: string
    determinationType: string
    thirdPartyContentCondition: string
}

export interface ExportControl {
    isExportControl: string
    ear: string
    itar: string
}

export interface AuthorAffiliation {
    sequence: number
    submissionId: number
    meta: AuthorMeta
    id: string
}

export interface AuthorMeta {
    author: Author
    organization: Organization
}

export interface Author {
    name: string
}

export interface Organization {
    name: string
    location: string
}

export interface LegacyMeta {
    __type: string
    accessionNumber: string
}

export interface Center {
    code: string
    name: string
    id: string
}

export interface Meeting {
    submissionId: number
    endDate: string
    name: string
    location: string
    id: string
    startDate: string
}

export interface Publication {
    issue?: number
    volume?: string,
    submissionId: number,
    publisher?: string,
    id: string,
    publicationName?: string,
    publicationDate: string
    issuePublicationDate?: string
}

export interface Aggregations {
    [key: string]: Aggregation
}

export interface Aggregation {
    doc_count_error_upper_bound?: number
    sum_other_doc_count?: number
    buckets: Bucket[]
}

export interface Bucket {
    key_as_string?: string
    key: number
    doc_count: number
}
