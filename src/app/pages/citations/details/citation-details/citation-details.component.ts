import { Component, Input, OnInit } from '@angular/core';
import { CitationsService } from '../../citations.service';
import { Citation } from '../../models';
import { Meta, Title } from '@angular/platform-browser'
import { NbDateFnsDateService } from '@nebular/date-fns';
import { Location } from '@angular/common';

@Component({
  selector: 'app-citation-details',
  templateUrl: './citation-details.component.html',
  styleUrls: ['./citation-details.component.scss']
})
export class CitationDetailsComponent implements OnInit {
  citation!: Citation | undefined;
  private citationId: number | null = null;
  constructor(private citationService: CitationsService, private title: Title, private meta: Meta, private dateService: NbDateFnsDateService, private location: Location) { }

  ngOnInit(): void {

    this.citationService.getCitation(this.citationId).subscribe(citation => {
      if (citation) {
        this.title.setTitle(citation.title)
        this.meta.updateTag({ name: 'description', content: 'Online document viewer and detailed document description' })
        this.citation = citation;
      }
    })
  }

  @Input()
  set id(citationId: number) {
    this.citationId = citationId;
  }

  navigateBack = () => this.location.back();

  getPdfUrl = (): string => this.citation?.downloads?.find(d => d.links.pdf)?.links.pdf || "";

  getAuthors = (citation: Citation) => citation.authorAffiliations.map(af => af.meta.author.name).join(', ')

  getFormattedDate = (date: string) => this.dateService.format(new Date(date), 'MMMM d, yyyy')

  getDownloadFilename = () => {
    const fileName = this.citation?.title.replace(/\s+/g, '_')
    return fileName + '.pdf';
  }

  hasPdf = () => this.citation?.downloads?.find(d => d.links.pdf);
}
