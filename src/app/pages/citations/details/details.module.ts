import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitationDetailsComponent } from './citation-details/citation-details.component';
import { NbButtonModule, NbIconModule, NbLayoutModule } from '@nebular/theme';
import { ComponentsModule } from 'src/app/components/components.module';



@NgModule({
  declarations: [
    CitationDetailsComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule,
    NbIconModule,
    NbLayoutModule,
    NbButtonModule
  ]
})
export class DetailsModule { }
